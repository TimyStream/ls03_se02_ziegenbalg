import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class view {
    public static void main(String[] args) {
        JFrame frame = new JFrame("view");
        frame.setContentPane(new view().panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private JPanel panel1;
    private JButton button1;

    public view() {
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                buttonRed_clicked();
            }
        });
    }

    public  void buttonRed_clicked() {
        if(this.panel1.getBackground() == Color.RED) {
            this.panel1.setBackground(Color.CYAN);
        } else {
            this.panel1.setBackground(Color.RED);
        }
    }
}
